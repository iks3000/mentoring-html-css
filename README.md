## Mentoring-HTML-CSS
### Stanislav Khomyn
---
### Achievements:
* Saved time for the company
* Received awards
* Been complimented by your supervisor or co-workersgit status

### Common Git Commands
* git init
* git clone < remote_URL >
* git branch < branch_name >
* git checkout < branch_name >
* git add .  (To add all files not staged)
* git add < file or directory name >
* git commit -m "Commit message"
* git push < remote_URL/remote_name > < branch >
* git status
* git log
* git pull < branch_name > < remote_URL/remote_name >
* git cherry-pick < SHA >
